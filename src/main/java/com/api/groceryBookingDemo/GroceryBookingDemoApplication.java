package com.api.groceryBookingDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroceryBookingDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroceryBookingDemoApplication.class, args);
	}

}
