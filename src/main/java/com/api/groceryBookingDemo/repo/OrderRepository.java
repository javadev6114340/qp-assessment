package com.api.groceryBookingDemo.repo;

import com.api.groceryBookingDemo.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Long> {
}
