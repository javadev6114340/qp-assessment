package com.api.groceryBookingDemo.repo;

import com.api.groceryBookingDemo.entity.GroceryItems;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroceryItemRepository extends JpaRepository<GroceryItems,Long> {

     List<GroceryItems> findByName(String name);
     List<GroceryItems> findByInventoryGreaterThan(int availableInventory);
}
