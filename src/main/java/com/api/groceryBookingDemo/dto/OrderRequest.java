package com.api.groceryBookingDemo.dto;

import java.util.List;

public class OrderRequest {

    private int userId;
    private List<Long> groceryItemIds;

    public OrderRequest(){}

    public OrderRequest(int userId, List<Long> groceryItemIds) {
        this.userId = userId;
        this.groceryItemIds = groceryItemIds;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Long> getGroceryItemIds() {
        return groceryItemIds;
    }

    public void setGroceryItemIds(List<Long> groceryItemIds) {
        this.groceryItemIds = groceryItemIds;
    }




}
