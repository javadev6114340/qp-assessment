package com.api.groceryBookingDemo.service;

import com.api.groceryBookingDemo.dto.OrderRequest;
import com.api.groceryBookingDemo.entity.GroceryItems;
import com.api.groceryBookingDemo.entity.Order;
import com.api.groceryBookingDemo.entity.UserInfo;
import com.api.groceryBookingDemo.repo.GroceryItemRepository;
import com.api.groceryBookingDemo.repo.OrderRepository;
import com.api.groceryBookingDemo.repo.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private GroceryItemRepository groceryItemRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    public void placeOrder(OrderRequest orderRequest) {
        // Retrieve the user and grocery items from the request

        UserInfo user = userInfoRepository.findById(orderRequest.getUserId()).orElse(null);
        List<GroceryItems> orderedItems = groceryItemRepository.findAllById(orderRequest.getGroceryItemIds());

        if (user == null) {
            throw new RuntimeException("Invalid user: User not found");
        }

        if (orderedItems.isEmpty()) {

            throw new RuntimeException("Invalid order: No grocery items selected");
        }

        Order order = new Order();
        order.setUser(user);
        order.setOrderDate(new Date());
        order.setOrderedItems(orderedItems);

        orderRepository.save(order);
    }
}
