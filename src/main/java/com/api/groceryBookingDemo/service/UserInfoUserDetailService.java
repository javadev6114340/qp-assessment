package com.api.groceryBookingDemo.service;

import com.api.groceryBookingDemo.config.UserInfoUserDetail;
import com.api.groceryBookingDemo.entity.UserInfo;
import com.api.groceryBookingDemo.repo.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserInfoUserDetailService implements UserDetailsService {

    @Autowired
    private UserInfoRepository userInfoRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserInfo> userInfo= userInfoRepository.findByUsername(username);
        return userInfo.map(UserInfoUserDetail::new)
        .orElseThrow(()->new UsernameNotFoundException("User not found"));

    }
}
