package com.api.groceryBookingDemo.service;

import com.api.groceryBookingDemo.entity.GroceryItems;
import com.api.groceryBookingDemo.repo.GroceryItemRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroceryItemService {
    @Autowired
    private GroceryItemRepository groceryItemRepository;

    //Both are POST method
    public GroceryItems addGroceryItem(GroceryItems groceryItems){

        return groceryItemRepository.save(groceryItems);
    }

    public List<GroceryItems> addGroceryItems(List<GroceryItems> groceryItemsList){
        return groceryItemRepository.saveAll(groceryItemsList);
    }

    //GET methid
    public List<GroceryItems> getAllGroceryItems(){

        return groceryItemRepository.findAll();
    }
    public GroceryItems getGroceryItemById(Long id){

        return groceryItemRepository.findById(id).orElse(null);
    }
    public List<GroceryItems> getGroceryItemByName(String name){
        return groceryItemRepository.findByName(name);
    }
    //DELETE method
    public String removeGroceryItem(Long itemId){
        groceryItemRepository.deleteById(itemId);
        return "Item removed"+itemId;
    }
    //PUT method
    public GroceryItems updateGroceryItems(Long itemId,GroceryItems updateItem){
        GroceryItems existingItem = groceryItemRepository.findById(itemId)
                .orElseThrow(() ->new EntityNotFoundException("Item not found"));
        existingItem.setName(updateItem.getName());
        existingItem.setPrice(updateItem.getPrice());
        existingItem.setInventory(updateItem.getInventory());
        return groceryItemRepository.save(existingItem);
    }
    public List<GroceryItems> getAvailableItems() {
        // Define the threshold for available items (e.g., inventory > 0)
        int availableInventory = 0;
        return groceryItemRepository.findByInventoryGreaterThan(availableInventory);
    }

}
