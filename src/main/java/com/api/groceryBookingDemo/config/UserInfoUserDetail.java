package com.api.groceryBookingDemo.config;

import com.api.groceryBookingDemo.entity.UserInfo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserInfoUserDetail implements UserDetails {

    private String username;
    private String password;
    private List<GrantedAuthority> authorites;

    public UserInfoUserDetail(UserInfo userInfo){
        username= userInfo.getUsername();
        password=userInfo.getPassword();
        authorites= Arrays.stream(userInfo.getRole().split(","))
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());

    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorites;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
