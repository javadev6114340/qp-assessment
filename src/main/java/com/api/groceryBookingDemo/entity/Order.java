package com.api.groceryBookingDemo.entity;



import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="ORDER_DETAILS")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="User_id" ,referencedColumnName="id")
    private UserInfo user;
    private Date orderDate;
    @OneToMany(fetch=FetchType.EAGER,cascade = CascadeType.ALL,targetEntity = GroceryItems.class)
    @JoinColumn(name="order_id",referencedColumnName = "id")
    private List<GroceryItems> orderedItems;

    public Order(){}
    public Order(UserInfo user, Date orderDate, List<GroceryItems> orderedItems) {
        this.user = user;
        this.orderDate = orderDate;
        this.orderedItems = orderedItems;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public List<GroceryItems> getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(List<GroceryItems> orderedItems) {
        this.orderedItems = orderedItems;
    }


}
