package com.api.groceryBookingDemo.controller;

import com.api.groceryBookingDemo.dto.AuthRequest;
import com.api.groceryBookingDemo.entity.UserInfo;
import com.api.groceryBookingDemo.service.JWTService;
import com.api.groceryBookingDemo.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private JWTService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/addUser")
    public String addUser(@RequestBody UserInfo userInfo){
        return userInfoService.addUser(userInfo);

    }

    @PostMapping("/authenticate")
    public String autheticateAndGetToken(@RequestBody  AuthRequest authRequest){
       Authentication authentication= authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(),authRequest.getPassword()));
       if(authentication.isAuthenticated()){
           return jwtService.generateToken(authRequest.getUsername());
       }else{
           throw new UsernameNotFoundException("Invalid user request");
       }

    }
}
