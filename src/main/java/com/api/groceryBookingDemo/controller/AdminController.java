package com.api.groceryBookingDemo.controller;

import com.api.groceryBookingDemo.dto.OrderRequest;
import com.api.groceryBookingDemo.entity.GroceryItems;
import com.api.groceryBookingDemo.service.GroceryItemService;
import com.api.groceryBookingDemo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AdminController {

    @Autowired
    private GroceryItemService groceryItemService;
    @Autowired
    private OrderService orderService;

    @GetMapping("/welcome")
    public ResponseEntity<String> adminUser(){
        return ResponseEntity.ok("Welcome this is not secure endpoint");
    }
    @PostMapping("/grocery/addSingle-items")
    @PreAuthorize("hasAuthority('ADMIN')")
    public GroceryItems addItems(@RequestBody GroceryItems groceryItems){
        return groceryItemService.addGroceryItem(groceryItems);
    }

    @PostMapping("/grocery/addAll")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<GroceryItems> addItemList(@RequestBody List<GroceryItems> groceryItemsList){
        return groceryItemService.addGroceryItems(groceryItemsList);
    }
    @GetMapping("/grocery/itemsAll")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<GroceryItems> getAllItems(){
        return groceryItemService.getAllGroceryItems();
    }
    //we need to pass id as part of request url so @PathVariable or @RequestParam
    //Since we have used @PathVariable we must pass id otherwise we will get 404
    //but for @RequestParam it's not necessary
    @GetMapping("/grocery/items/{id}")
    @PreAuthorize("hasAuthority('USER')")
    public GroceryItems getItemsById(@PathVariable Long id){
        return groceryItemService.getGroceryItemById(id);
    }

    @GetMapping("/grocery/items/{name}")
    @PreAuthorize("hasAuthority('USER')")
    public List<GroceryItems> getItemsByName(@PathVariable  String name){
        return groceryItemService.getGroceryItemByName(name);
    }

    @PutMapping("/grocery/update/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public GroceryItems updateItems (@PathVariable  Long id, @RequestBody   GroceryItems groceryItems){
        return groceryItemService.updateGroceryItems(id,groceryItems);
    }

    @DeleteMapping("/grocery/delete/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String removeItem(@PathVariable Long id)
    {
        return groceryItemService.removeGroceryItem(id);
    }

    @GetMapping("/grocery/available-items")
    @PreAuthorize("hasAuthority('USER')")
    public List<GroceryItems> getAvailableItems() {
        return groceryItemService.getAvailableItems();
    }

    @PostMapping("/grocery/place-order")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<String> placeOrder(@RequestBody OrderRequest orderRequest) {
        orderService.placeOrder(orderRequest);

        return ResponseEntity.ok("Order placed successfully");
    }
}
